from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render
from django.contrib.auth.forms import AuthenticationForm
from .forms import contact_forms
from django.core.mail import  send_mail, BadHeaderError
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt,csrf_protect

# Create your views here.
@csrf_exempt
def index(request):
    if request.method == 'POST':
        form = AuthenticationForm(data= request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('siswa/home-siswa')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form':form})

# codes below are get from https://www.ordinarycoders.com/blog/article/build-a-django-contact-form-with-email-backend
def contact_us(request):
    if request.method == 'POST':
        form = contact_forms(request.POST)
        if form.is_valid():
            subject = "Website Inquiry"
            body = {
                'first_name' : form.cleaned_data['first_name'],
                'last_name' : form.cleaned_data['last_name'],
                'email' : form.cleaned_data['email'],
                'message' : form.cleaned_data['message']
            }
            message = '\n'.join(body.values())

            try:
                send_mail(subject, message, settings.EMAIL_HOST_USER, ["tugaskelompokpbp@gmail.com"])
            except BadHeaderError:
                return HttpResponse ("Invalid Header Found/error")
            return redirect('index')
    form = contact_forms()
    return render (request, 'contact.html', {'form' : form})

def about_us(request):
    return render (request, 'aboutus.html',{})