from django.urls import path
from .views import about_us, index, contact_us

urlpatterns = [
    path("", index, name="index"),
    path("contact-us", contact_us, name="contact"),
    path("about-us", about_us, name="about")
]
