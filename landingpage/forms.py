from django import forms


class contact_forms (forms.Form):
    first_name = forms.CharField(max_length=50, required=True)
    last_name = forms.CharField(max_length=50, required=True)
    email = forms.EmailField(required=True)
    message = forms.CharField(widget = forms.Textarea, max_length = 2000, required=True)
        