from django.apps import AppConfig


class CreatequizesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'createQuizes'
