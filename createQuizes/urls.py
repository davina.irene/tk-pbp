from django.urls import path
from .views import createQuestions, createQuiz, createQuestionsSimple, createQuestionsSimpleFlutter, viewSimpleQuestion, nuke

urlpatterns = [
    path('quiz', createQuiz, name='createQuiz'),
    path('question', createQuestions, name='createQuiz'),
    path('questionSimple', createQuestionsSimple, name='createQuizSimple'),
    path('questionSimpleFlutter', createQuestionsSimpleFlutter, name='createQuestionsSimpleFlutter'),
    path('viewsimple',viewSimpleQuestion,name='viewSimpleQuestion'),
    path('nuke',nuke,name='nuke'),
]
