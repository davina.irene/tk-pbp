from django import forms
from quizes.models import Quiz
from questions.models import Question, QuestionSimple

class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = "__all__"

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = "__all__"

class QuestionSimpleForm(forms.ModelForm):
    class Meta:
        model = QuestionSimple
        fields = "__all__"
