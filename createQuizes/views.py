from django.shortcuts import render, redirect
from .forms import QuestionForm, QuizForm, QuestionSimpleForm
from quizes.models import Quiz
from questions.models import QuestionSimple
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers
from django.http import HttpResponse

# Create your views here.
# TODO:import user
def createQuiz(request):
    # user = User.objects.get(username=request.user.username)
    #TODO :cari tahu cara sisipkan user dalam form
    context = {}

    form = QuizForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            #TODO: cari nama url sebelumnya
            # return redirect('')
            pass
        

    context['form']= form
    return render(request, "add_quiz.html", context)

def createQuestions(request):
    #from https://stackoverflow.com/questions/12829085/getting-a-foreign-key-into-a-form
    #TODO:sesuai parameter dari html
    context = {}
    # id = request.GET.get( 'quiz-id' )

    # quiz = Quiz.objects.get( pk=id )
    # form = QuestionForm( initial={ 'Quiz':record.id } , request.POST or None, request.FILES or None)
    form = QuestionForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            #TODO: cari nama url sebelumnya
            # return redirect('')
            pass
        

    context['form']= form
    return render(request, "add_question.html", context)

def createQuestionsSimple(request):
    #from https://stackoverflow.com/questions/12829085/getting-a-foreign-key-into-a-form
    #TODO:sesuai parameter dari html
    context = {}
    # id = request.GET.get( 'quiz-id' )

    # quiz = Quiz.objects.get( pk=id )
    # form = QuestionForm( initial={ 'Quiz':record.id } , request.POST or None, request.FILES or None)
    form = QuestionSimpleForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            #TODO: cari nama url sebelumnya
            # return redirect('')
            pass
        

    context['form']= form
    return render(request, "add_questionSimple.html", context)

@csrf_exempt
def createQuestionsSimpleFlutter(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        question = data["question"]

        op1 = data["op1"]
        op2 = data["op2"]
        op3 = data["op3"]
        op4 = data["op4"]
        ans = data["ans"]

        pembahasan = data["pembahasan"]

        form = QuestionSimple.objects.create(
            question = question,
            op1 = op1,
            op2 = op2,
            op3 = op3,
            op4 = op4,
            ans = ans,
            pembahasan = pembahasan
        )
        
        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)


def viewSimpleQuestion(request):
    questionSimple = QuestionSimple
    data = serializers.serialize('json', questionSimple.objects.all())
    return HttpResponse(data, content_type="application/json")

def nuke(request):
    QuestionSimple.objects.all().delete()