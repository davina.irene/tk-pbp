from django.http import request
from django.shortcuts import redirect, render
from main.models import User, Question

def home(request):
    return render(request, 'main/home.html')
