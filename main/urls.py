from django.urls import path

from landingpage.views import index

from . import views

app_name = 'main'

urlpatterns = [
     path('', index, name='home')
]
