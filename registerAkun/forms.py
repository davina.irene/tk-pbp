from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser
from django import forms
class CustomUserCreationForm(UserCreationForm):
    class Meta:
        
        student = forms.ChoiceField(choices=["Student", "Teacher"], required=True)
        model = CustomUser
        fields = ('email',"student")
class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('email',)