from django.apps import AppConfig


class RegisterAkunConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'registerAkun'
