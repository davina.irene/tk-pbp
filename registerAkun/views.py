from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.contrib import messages
from .forms import CustomUserCreationForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration Successful")
            return redirect("/")
        messages.error(request, "Registration unsuccessful, please fill all the column with valid information")
    else:
        form = CustomUserCreationForm()
    return render(request, 'regisAkun.html', {'form':form})
