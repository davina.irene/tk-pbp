"""kuiskuy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

admin.autodiscover()

import siswa.urls as siswa
import pengajar.urls as pengajar
import page_pemabahasan.urls as pembahasan
import landingpage.urls as landingpage
import questions.urls as questions
import registerAkun.urls as register
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', include('main.urls')),
    path('siswa/', include(siswa)),
    path('pengajar/', include(pengajar)),
    path('create/', include('createQuizes.urls')),
    path('pembahasan/', include(pembahasan)),
    path('', include(landingpage)),
    path('kuis/', include(questions)),
    path("register/", include(register)),
    # url(r'^$','testapp.views.home'),
    # url(r'^admin/',include(admin.site.urls)),
]

urlpatterns += staticfiles_urlpatterns()
