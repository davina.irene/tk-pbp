from django.urls import path

from .  import views


urlpatterns = [
    path('hasil_kuis/', views.hasil_kuis, name='hasil_kuis'),
    path('', views.kuis, name='kuis'),
]
