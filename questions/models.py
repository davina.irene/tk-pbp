from django.db import models
from quizes.models import Quiz
# Create your models here.

class Question(models.Model):
    question = models.CharField(max_length=200,null=True)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, default=1)

    op1 = models.CharField(max_length=200,null=True)
    op2 = models.CharField(max_length=200,null=True)
    op3 = models.CharField(max_length=200,null=True)
    op4 = models.CharField(max_length=200,null=True)
    ans = models.CharField(max_length=200,null=True)

    pembahasan = models.CharField(max_length=200,null=True)
    
    def __str__(self):
        return self.question
class QuestionSimple(models.Model):
    question = models.CharField(max_length=200,null=True)

    op1 = models.CharField(max_length=200,null=True)
    op2 = models.CharField(max_length=200,null=True)
    op3 = models.CharField(max_length=200,null=True)
    op4 = models.CharField(max_length=200,null=True)
    ans = models.CharField(max_length=200,null=True)

    pembahasan = models.CharField(max_length=200,null=True)
    
    def __str__(self):
        return self.question
