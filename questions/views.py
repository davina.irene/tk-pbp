from django.shortcuts import render
from .models import *

# Create your views here.x
def kuis(request):
    if request.method == 'POST':
        print (request.POST)
        #deleted .values
        questions = Question.objects.all()
        score=0
        wrong=0
        correct=0
        total=0
        for q in questions:
            total+=1
            #fix: mengganti nama field di questions
            print(request.POST.get(q.question))
            pick =request.POST.get(q.question)
            if pick == "option1":
                thisAns = q.op1
            elif pick == "option2":
                thisAns = q.op2
            elif pick == "option3":
                thisAns = q.op3
            else :
                thisAns = q.op4
            print(thisAns)
            print(q.ans)
            print()


            if thisAns == q.ans:
                score+=10
                correct+=1
            else:
                wrong+=1
        totalNilai = score/(total*10) * 100
        context = {
        'score':score,
        'correct':correct,
        'wrong':wrong,
        'total':total,
        }
        return render(request, 'hasil.html', context)
    else:
        questions = Question.objects.all()
        context = {
            'questions' : questions,
        }
        return render(request, 'home.html', context)

def hasil_kuis(request):
    return render(request, 'hasil.html')