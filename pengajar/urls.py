from django.urls import path

from . import views



urlpatterns = [
    path('home_pengajar', views.home_pengajar, name='home_pengajar'),
    path('kumpulan_kuis', views.kumpulan_kuis, name='kumpulan_kuis'),
    path('profile', views.profile, name="profile")
]

