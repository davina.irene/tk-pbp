from django.http import request
from django.shortcuts import redirect, render


def home_pengajar(request):
    return render(request, 'homepengajar.html')

def kumpulan_kuis(request):
    return render(request, 'kumpulankuis.html')

def profile(request):
    response = {'ct' : range(25)}
    return render(request, 'profile-pengajar.html', response)
# def is_teacher(user):
#     return user.groups.filter(name='pengajar').exists()