from django import forms
from quizes import models

class TokenForm(forms.ModelForm):
    class Meta:
        model = models.Quiz
        fields = "__all__"