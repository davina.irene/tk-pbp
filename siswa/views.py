from django.http import request
from django.shortcuts import redirect, render


def home_siswa(request):
    return render(request, 'homesiswa.html')

def profile(request):
    response = {'ct' : range(25)}
    return render(request, 'profile-siswa.html', response)
