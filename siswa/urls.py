from django.urls import path

from . import views




urlpatterns = [
    path('home-siswa', views.home_siswa, name='home_siswa'),
    path('profile', views.profile, name="profile-siswa")
]

